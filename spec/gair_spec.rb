# coding: utf-8
#require File.dirname(__FILE__) + '/../lib/gair'
require_relative '../lib/gair'
require 'rspec'

describe "gair" do
  it "should give cytseiniaid" do
    geiriau = {
      "cortyn" => %w{c r t n},
      "pwerus" => %w{p r s},
      "corff" => %w{c r ff},
      "dyddiadur" => %w{d dd d r},
      "tanbaid" => %w{t n b d}
    }
    generic_test geiriau, :cytseiniaid
  end

  it "should give llafariaid" do
    geiriau = {
      "trwm" => %w{w},
      "clên" => %w{ê},
      "byd" => %w{y},
      "llef" => %w{e},
      "car" => %w{a},
      "câr" => %w{â},
      "llyn" => %w{y},
      "punt" => %w{u}
    }
    generic_test geiriau, :llafariaid
  end

  it "should give sillafau" do
    geiriau = {
      "gweledigaeth" => 4,
      "pur" => 1,
      "crechwen" => 2,
      "perfedd" => 2,
      "tymheredd" => 3,
      "meddwdod" => 3,
      #"gwladwriaeth" => 3,
      "marwnad" => 3,
      "enwau" => 2
    }
    generic_test geiriau, :sillafau
  end

  def generic_test(geiriau,test)
    geiriau.each do |k,v|
      gair = Gair.new k
      gair.send(test).should == v
    end
  end
end  
