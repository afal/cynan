# coding: utf-8
class Gair
  attr_accessor :gair

  LLAFARIAID = %w{a e i o u w y â ê î ô û ŵ ŷ}
  CYTSEINIAID = %w{b c ch d dd f ff g ng h j l ll m n p ph r rh s t th}
  LLYTHRENNAU_DWBL = %w{dd ff ng ll ph rh th}

  def initialize(gair)
    @gair = gair
  end
 
  def cytseiniaid
    llythrennau.find_all{|llythyren| CYTSEINIAID.include? llythyren }
  end

  def llafariaid
    llythrennau.find_all{|llythyren| LLAFARIAID.include? llythyren }
  end

  def sillafau
    @gair.split(/[#{CYTSEINIAID.join}]/).find_all{|x| x != ""}.length
  end

  def llythrennau
    llythrennau = []
    pointer = 0
    while pointer < @gair.length
      ll = @gair[pointer,2]
      plus = (LLYTHRENNAU_DWBL.include?(ll) or LLAFARIAID.include?(ll)) ? 2 : 1
      llythrennau << @gair[pointer,plus]
      pointer += plus
    end
    llythrennau
  end
end
