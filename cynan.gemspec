Gem::Specification.new do |s|
  s.name    = 'cynan'
  s.version = '0.0.1'
  s.summary = 'A cynghanedd parser'

  s.author   = 'Dafydd Francis'
  s.email    = 'afal@somethingafal.com'
  s.homepage = 'https://bitbucket.org/afal/cynan'

  # These dependencies are only for people who work on this gem
  s.add_development_dependency 'rspec'

  # Include everything in the lib folder
  s.files = Dir['lib/**/*']

  # Supress the warning about no rubyforge project
  s.rubyforge_project = 'nowarning'
end
